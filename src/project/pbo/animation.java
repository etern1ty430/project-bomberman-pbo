/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author edwin
 */
class animasi_enemy_endless extends java.lang.Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon [][] gambar = new ImageIcon[4][3];
    int ctrGerak,cmd;
    LOADING_SCREEN load=new LOADING_SCREEN();
    player player;
    int temp;
    NewJFrame b=null;
    public animasi_enemy_endless(JLabel lblUnit, int cmd, ImageIcon[][] gambar,LOADING_SCREEN load) {
        this.load=load;
        this.lblUnit = lblUnit;
        this.ctrGerak = 0;
        this.gambar = gambar;
        this.cmd = cmd;
        this.temp=0;
        this.player=player;
        this.b=(NewJFrame) load.getParent().getParent().getParent().getParent();
    }



    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        
        while(ctrGerak < 5){
            if(ctrGerak == 5){
                this.lblUnit.setIcon(gambar[cmd][1]);
            }
            else if(ctrGerak %2 == 0){
                this.lblUnit.setIcon(gambar[cmd][0]);
            }
            else if(ctrGerak %2 != 0){
                this.lblUnit.setIcon(gambar[cmd][2]);
            }
            ctrGerak++;
            setLokasi();
            
            try {
                Thread.sleep(25);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
        if(!lblUnit.getIcon().equals("")){
            b.ENDLESS.player.life-=temp/5;
        }
    }
    public void setLokasi(){
        if(cmd==0 && cekKosong(lblUnit.getX(), lblUnit.getY()-50)==true){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() - 10);
        }
        else if(cmd==1 && cekKosong(lblUnit.getX(), lblUnit.getY()+50)==true){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() + 10);
        }
        else if(cmd==2 && cekKosong(lblUnit.getX()-50, lblUnit.getY())==true){
            lblUnit.setLocation(lblUnit.getX() - 10, lblUnit.getY());
            lblUnit.setIcon(gambar[2][2]);
        }
        else if(cmd==3 && cekKosong(lblUnit.getX()+50, lblUnit.getY())==true){
            lblUnit.setLocation(lblUnit.getX() + 10, lblUnit.getY());
        }
        
    }
    boolean cekKosong(int x, int y){
        if (b.ENDLESS.player.lblUnit.getX()==x && b.ENDLESS.player.lblUnit.getY()==y){
            temp++;
            return false;
        }
        for (rock object : b.ENDLESS.batu) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            }     
        }
        for (tree object : b.ENDLESS.pepohonan) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            } 
        }
        for (unit object : b.ENDLESS.enemy) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            }
                
        }
        return true;
    }
}

class animasi_enemy_stage extends java.lang.Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon [][] gambar = new ImageIcon[4][3];
    int ctrGerak,cmd;
    LOADING_SCREEN load=new LOADING_SCREEN();
    int temp;
    NewJFrame b=null;
    public animasi_enemy_stage(JLabel lblUnit, int cmd, ImageIcon[][] gambar,LOADING_SCREEN load) {
        this.load=load;
        this.lblUnit = lblUnit;
        this.ctrGerak = 0;
        this.gambar = gambar;
        this.cmd = cmd;
        this.temp=0;
        this.b=(NewJFrame) load.getParent().getParent().getParent().getParent();
    }



    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        
        while(ctrGerak < 5){
            if(ctrGerak == 5){
                this.lblUnit.setIcon(gambar[cmd][1]);
            }
            else if(ctrGerak %2 == 0){
                this.lblUnit.setIcon(gambar[cmd][0]);
            }
            else if(ctrGerak %2 != 0){
                this.lblUnit.setIcon(gambar[cmd][2]);
            }
            ctrGerak++;
            setLokasi();
            
            try {
                Thread.sleep(25);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
        if(!lblUnit.getIcon().equals("")){
            b.player.life-=temp/5;
        }
    }
    public void setLokasi(){
        if(cmd==0 && cekKosong(lblUnit.getX(), lblUnit.getY()-50)==true){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() - 10);
        }
        else if(cmd==1 && cekKosong(lblUnit.getX(), lblUnit.getY()+50)==true){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() + 10);
        }
        else if(cmd==2 && cekKosong(lblUnit.getX()-50, lblUnit.getY())==true){
            lblUnit.setLocation(lblUnit.getX() - 10, lblUnit.getY());
            lblUnit.setIcon(gambar[2][2]);
        }
        else if(cmd==3 && cekKosong(lblUnit.getX()+50, lblUnit.getY())==true){
            lblUnit.setLocation(lblUnit.getX() + 10, lblUnit.getY());
        }
        
    }
    boolean cekKosong(int x, int y){
        if (b.player.lblUnit.getX()==x && b.player.lblUnit.getY()==y){
            temp++;
            return false;
        }
        for (rock object : b.STAGE.rock) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            }     
        }
        for (tree object : b.STAGE.tree) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            } 
        }
        for (unit object : b.STAGE.unit) {
            if (object.lblUnit.getX()==x&&object.lblUnit.getY()==y){
                return false;
            }
                
        }
        return true;
    }
}



class animasi_player extends java.lang.Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon [][] gambar = new ImageIcon[4][3];
    int ctrGerak,cmd;

    public animasi_player(JLabel lblUnit, int cmd, ImageIcon[][] gambar) {
        this.lblUnit = lblUnit;
        this.ctrGerak = 0;
        this.gambar = gambar;
        this.cmd = cmd;
    }



    @Override
    public void run() {
        //super.run(); //To change body of generated methods, choose Tools | Templates.
        while(ctrGerak < 5){
            if(ctrGerak == 5){
                this.lblUnit.setIcon(gambar[cmd][1]);
            }
            else if(ctrGerak %2 == 0){
                this.lblUnit.setIcon(gambar[cmd][0]);
            }
            else if(ctrGerak %2 != 0){
                this.lblUnit.setIcon(gambar[cmd][2]);
            }
            ctrGerak++;
            setLokasi();
            
            try {
                Thread.sleep(25);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
    }
    public void setLokasi(){
        if(cmd==0 && lblUnit.getY()>0){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() - 10);
        }
        else if(cmd==1 && lblUnit.getY()<500){
            lblUnit.setLocation(lblUnit.getX(), lblUnit.getY() + 10);
        }
        else if(cmd==2 && lblUnit.getX()>0){
            lblUnit.setLocation(lblUnit.getX() - 10, lblUnit.getY());
            lblUnit.setIcon(gambar[2][2]);
        }
        else if(cmd==3 && lblUnit.getX()<700){
            lblUnit.setLocation(lblUnit.getX() + 10, lblUnit.getY());
        }
    }
}