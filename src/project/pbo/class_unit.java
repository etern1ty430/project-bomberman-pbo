/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author edwin
 */
class unit extends Thread implements Serializable{
    JLabel lblUnit;
    ImageIcon gambar;
    int x;
    int y;
    int life;
    int cmd;
    
    public unit(int x,int y){
        this.x = x;
        this.y = y;
        this.cmd=0;
        this.gambar=gambar;
        this.lblUnit=lblUnit;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }
    
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
}
class player extends unit implements Runnable,Serializable{
    ImageIcon[][] gambarx = new ImageIcon[4][3];
    animasi_player anim;
    int score=0;
    public player(int x, int y) {
        super(x, y);
        setLife(3);
        ImageIcon raw = new ImageIcon("image/unit/player/pup1.png");
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pup2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pup3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pdown1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pdown2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pdown3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pleft1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pleft2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pleft3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pright1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pright2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/player/pright3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[0][0]);
        this.lblUnit.setBounds(x, y, 50, 50);
    } 
    public void goLeft(){
        cmd=2;
    }
    public void goRight(){
        cmd=3;
    }
    public void goUp(){
        cmd=0;
    }
    public void goDown(){
        cmd=1;
    }
    public void kurangHP(){
        setLife(getLife()-1);
    }
    @Override
    public void run(){
        anim=new animasi_player(lblUnit, cmd, gambarx);
        anim.start();
    }
}
class enemy_endless extends unit implements Runnable,Serializable {
    ImageIcon[][] gambarx = new ImageIcon[4][3];
    animasi_enemy_endless anim;
    player player;
    LOADING_SCREEN load=new LOADING_SCREEN();
    public enemy_endless(int x, int y,LOADING_SCREEN load) {
        super(x, y);
        this.load=load;
        ImageIcon raw = new ImageIcon("image/unit/enemy/eup1.png");
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eup2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eup3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[1][0]);
        this.lblUnit.setBounds(x, y, 50, 50);
        
    } 

    @Override
    public void run() {
        

        while(true){
            int cmd=(int) (Math.random()*4);
            anim = new animasi_enemy_endless(lblUnit,cmd,gambarx,load);
            anim.start();
            

            try {
                Thread.sleep(1000);//miliseconds
            } catch (InterruptedException ex) {
                
            }
        }
        
    }
}

class enemy_stage extends unit implements Runnable,Serializable {
    ImageIcon[][] gambarx = new ImageIcon[4][3];
    animasi_enemy_stage anim;
    LOADING_SCREEN load=new LOADING_SCREEN();
    public enemy_stage(int x, int y,LOADING_SCREEN load) {
        super(x, y);
        this.load=load;
        ImageIcon raw = new ImageIcon("image/unit/enemy/eup1.png");
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eup2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eup3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/edown3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eleft3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/enemy/eright3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[1][0]);
        this.lblUnit.setBounds(x, y, 50, 50);
        
    } 

    @Override
    public void run() {
        

        while(true){
            int cmd=(int) (Math.random()*4);
            anim = new animasi_enemy_stage(lblUnit,cmd,gambarx,load);
            anim.start();
            

            try {
                Thread.sleep(1000);//miliseconds
            } catch (InterruptedException ex) {
                
            }
        }
        
    }
}

class enemy_boss_stage extends unit implements Runnable,Serializable {
    ImageIcon[][] gambarx = new ImageIcon[4][3];
    animasi_enemy_stage anim;
    LOADING_SCREEN load=new LOADING_SCREEN();
    public enemy_boss_stage(int x, int y,LOADING_SCREEN load) {
        super(x, y);
        this.load=load;
        ImageIcon raw = new ImageIcon("image/unit/boss/bup1.png");
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bup2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bup3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[0][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bdown1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bdown2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bdown3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[1][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bleft1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bleft2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bleft3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[2][2] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bright1.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][0] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bright2.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][1] = new ImageIcon(newImgx);
        
        raw = new ImageIcon("image/unit/boss/bright3.png");
        imgx = raw.getImage();
        newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
        this.gambarx[3][2] = new ImageIcon(newImgx);
        
        this.lblUnit = new JLabel(this.gambarx[1][0]);
        this.lblUnit.setBounds(x, y, 50, 50);
        
    } 

    @Override
    public void run() {
        

        while(true){
            int cmd=(int) (Math.random()*4);
            anim = new animasi_enemy_stage(lblUnit,cmd,gambarx,load);
            anim.start();
            

            try {
                Thread.sleep(500);//miliseconds
            } catch (InterruptedException ex) {
                
            }
        }
        
    }
}