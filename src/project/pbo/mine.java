/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author Steven
 */
public class mine extends Thread implements Runnable{
    JLabel lblUnit;
    STAGE_MODE stage;
    ImageIcon gambar;
    int x;
    int y;
    
    public mine(int x,int y,STAGE_MODE map){
        this.x=x;
        this.y=y;
        ImageIcon rawImage = new ImageIcon("image/bomb/mine.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
        
        this.stage=map;
    }
    
     public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
  
    @Override
    public void run() {
        boolean meledak = false;
        ArrayList<unit>toRemove2=new ArrayList<unit>();
        NewJFrame b=(NewJFrame) stage.getParent().getParent().getParent().getParent();
        
        do{
            try {
                for (unit x : b.STAGE.unit) {
                    if (x.lblUnit.getX()==this.lblUnit.getX() && x.lblUnit.getY()==this.lblUnit.getY()){
                        x.stop();
                        this.lblUnit.setIcon(new ImageIcon("image/bomb/explosion.png"));
                        b.STAGE.remove(x.lblUnit);
                        toRemove2.add(x);
                        meledak = true;
                    }
                }
                for(unit x:toRemove2){
                    b.STAGE.unit.remove(x);
                }
                Thread.sleep(500);
                if(meledak==true){
                    this.lblUnit.setIcon(new ImageIcon(""));
                }
                
            } catch (Exception e) {
            }
        }while(!meledak);
    
}
}
