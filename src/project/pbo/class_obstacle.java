/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.io.Serializable;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author edwin
 */
//obstacle

class tree implements Serializable{
        JLabel lblUnit;
        ImageIcon gambar;
        int x;
        int y;
    public tree(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon rawImage = new ImageIcon("image/map/Map1/tree.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
}

class rock implements Serializable{
    JLabel lblUnit;
    ImageIcon gambar;
    int x;
    int y;
    public rock(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon rawImage = new ImageIcon("image/map/Map1/rock1.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
}

class door implements Serializable{
    JLabel lblUnit;
    ImageIcon gambar;
    int x;
    int y;
    public door(int x, int y) {
        this.x = x;
        this.y = y;
        ImageIcon rawImage = new ImageIcon("image/map/Map1/door.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }
}