/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Yves
 */
public class endless_large extends javax.swing.JPanel implements Serializable{

    /**
     * Creates new form endless_large
     */
    JLabel lantai = new JLabel();
    ArrayList<unit> enemy = new ArrayList<unit>();
    ArrayList<tree> pepohonan = new ArrayList<tree>();
    ArrayList<rock> batu = new ArrayList<rock>();
    LOADING_SCREEN load=new LOADING_SCREEN();
    Random rnd = new Random();
    player player = new player(0,0);
    public endless_large(LOADING_SCREEN load) {
        initComponents();
        this.load=load;
        this.setSize(800,600);
        ImageIcon raw = new ImageIcon("image/map/Map3/Map003.png");
        Image imgx = raw.getImage();
        Image newImgx = imgx.getScaledInstance(800, 600, Image.SCALE_SMOOTH);
        lantai.setIcon(new ImageIcon(newImgx));
        lantai.setSize(800,600);

        for (int i = 0; i < 16; i++) {
            for (int j = 0; j < 11; j++) {
                if(i==1 && j==9){
                    player = new player(i*50,j*50);
                    this.add(player.lblUnit);
                } 
                else if(i==0 || j==0 || i==15 || j==10){
                    tree hard_wall = new tree(i*50,j*50);
                    hard_wall.lblUnit.setIcon(new ImageIcon("image/map/map4/crystal.png"));
                    this.add(hard_wall.lblUnit);
                    this.pepohonan.add(hard_wall);  
                }
            }
        }
        
        
        int counter = 0;
        int ply=0,plx=0;
        
        while(counter!=40){
            plx = rnd.nextInt(14)+1;
            ply = rnd.nextInt(9)+1;
            boolean chek = true;

            for (int i = 0; i < batu.size(); i++) {
                if(ply*50==batu.get(i).lblUnit.getY()&&plx*50==batu.get(i).lblUnit.getX()){
                    chek = false;
                    i = batu.size();
                }
            }

            if (chek==true){
                for (int i = 0; i < 16; i++) {
                    for (int j = 0; j < 11; j++) {
                        if(j==ply && i==plx && (plx!=1 && ply!=9)){
                            rock crystal = new rock(i*50,j*50);
                            raw = new ImageIcon("image/map/Map4/tentacle.png");
                            imgx = raw.getImage();
                            newImgx = imgx.getScaledInstance(50, 50, Image.SCALE_SMOOTH);
                            crystal.lblUnit.setIcon(new ImageIcon(newImgx));

                            this.add(crystal.lblUnit);
                            batu.add(crystal);
                            counter++;
                        }
                    }
                }
            }
        }
        //spawn enemy
        int counter_musuh = 0;
        while(counter_musuh<4){
            plx = rnd.nextInt(14)+1;
            ply = rnd.nextInt(9)+1;
            boolean chek = true;

            for (int i = 0; i < batu.size(); i++) {
                if(ply*50==batu.get(i).lblUnit.getY()&&plx*50==batu.get(i).lblUnit.getX()){
                    chek = false;
                    i = batu.size();
                }
            }
            if(!enemy.isEmpty()){
                for (int i = 0; i < enemy.size(); i++) {
                    if(ply*50==enemy.get(i).lblUnit.getY()&&plx*50==enemy.get(i).lblUnit.getX()){
                        chek = false;
                        break;
                    }
                }
            }
            

            if (chek==true){
                for (int i = 0; i < 16; i++) {
                    for (int j = 0; j < 11; j++) {
                        if(j==ply && i==plx && (plx!=1 && ply!=9)){
                            unit newmusuh = new enemy_endless(i*50,j*50,load);
                            enemy.add(newmusuh);
                            this.add(newmusuh.lblUnit);
                            counter_musuh++;
                        }
                    }
                }
            }
        }
        this.add(lantai);
    }

    public void respawnEnemy(){
            int plx = rnd.nextInt(14)+1;
            int ply = rnd.nextInt(9)+1;
            boolean chek = true;
            int counter = 0;
            int[] respawnX = new int[4];
            respawnX[0] = 3;
            respawnX[1] = 14;
            respawnX[2] = 3;
            respawnX[3] = 14;
            
            int[] respawnY = new int[4];
            respawnY[0] = 3;
            respawnY[1] = 3;
            respawnY[2] = 8;
            respawnY[3] = 8;
            
            
            while(counter!=2){
                
                /*
                if (chek==true){
                    for (int i = 0; i < 16; i++) {
                        for (int j = 0; j < 11; j++) {
                            if(j==ply && i==plx && (plx!=1 && ply!=9)){
                                unit newmusuh = new enemy_endless(i*50,j*50,load);
                                newmusuh.start();
                                enemy.add(newmusuh);
                                this.add(newmusuh.lblUnit,0);
                                this.repaint();
                                counter++;
                            }
                        }
                    }
                }*/
                    for (int i = 0; i < 4; i++) {
                        chek = true;
                        for (int j = 0; j < 16; j++) {
                            for (int k = 0; k < 11; k++) {
                                if(j==respawnY[i]&&k==respawnX[i]){
                                    if(j*50==batu.get(i).lblUnit.getY()&&k*50==batu.get(i).lblUnit.getX()){
                                        chek = false;
                                        i = batu.size();
                                    }
                                
                                
                                    if(!enemy.isEmpty()){
                                        for (int m = 0; m < enemy.size(); m++) {
                                            if(j*50==enemy.get(m).lblUnit.getY()&&k*50==enemy.get(m).lblUnit.getX()){
                                                chek = false;
                                                break;
                                            }
                                        }
                                    }

                                    if(chek==true){
                                        unit newmusuh = new enemy_endless(j*50,k*50,load);
                                        newmusuh.start();
                                        enemy.add(newmusuh);
                                        this.add(newmusuh.lblUnit,3);
                                        this.repaint();
                                        counter++;
                                        k=20;
                                        j=18;
                                        i=5;
                                    }
                                }
                                    
                                    
                                
                            }
                        }
                    }
                }
            
    }
    public void cek_life_and_score() {
        if(player.getLife()<=0){
            NewJFrame b= (NewJFrame) this.getParent().getParent().getParent().getParent();
            
            for(unit x:b.ENDLESS.enemy){
                x.stop();
            }
            String nama=JOptionPane.showInputDialog("ENTER YOUR NAME : ");
            
            System.out.println(nama);
            score x=new score();
            x.setNama(nama);
            x.setScore(player.score+10000+"");
            b.list_score.add(x);
            try {
            FileOutputStream fileOut = new FileOutputStream("score.txt");  // membuat file save
            ObjectOutputStream out = new ObjectOutputStream(fileOut);       // memilih file untuk save objek
            out.writeObject(b.list_score);                                        // save object
            out.close();                                                    // menutup oject stream
            fileOut.close();                                                // menutup file save
            JOptionPane.showMessageDialog(null, "Save score berhasil");
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Save gagal");
        }
            b.go_to_game_over();
        }
        life.setText(player.life+"");
        labelScore.setText(player.score+"");
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        labelScore = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        life = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("SCORE  : ");

        labelScore.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        labelScore.setForeground(new java.awt.Color(255, 255, 255));
        labelScore.setText("0");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("LIFE  :");

        life.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        life.setForeground(new java.awt.Color(255, 255, 255));
        life.setText("3");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(life)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 142, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelScore)
                .addGap(37, 37, 37))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(life)
                    .addComponent(jLabel1)
                    .addComponent(labelScore))
                .addGap(0, 267, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel labelScore;
    private javax.swing.JLabel life;
    // End of variables declaration//GEN-END:variables



    
}
