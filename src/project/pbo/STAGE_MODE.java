/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author edwin
 */
public class STAGE_MODE extends javax.swing.JPanel  implements Serializable{

    /**
     * Creates new form STAGE_MODE
     */
    
    door pintu=new door(0, 0);
    door shop=new door(0, 0);
    JLabel lantai=new JLabel();
    public static ArrayList<tree>tree=new ArrayList<tree>();
    ArrayList<rock>rock=new ArrayList<rock>();
    ArrayList<unit>unit=new ArrayList<unit>();
    LOADING_SCREEN load=new LOADING_SCREEN();
    public STAGE_MODE(player playerx,LOADING_SCREEN loadx,int stage,int gold) {
        initComponents();
        this.setSize(800, 600);
        this.load=loadx;
        this.GOLD_COUNT.setText(gold+"");
        this.LIFE_COUNT.setText(playerx.getLife()+"");
        this.STAGE_COUNT.setText(stage+"");
        this.add(GOLD_COUNT,0);
        this.add(LIFE_COUNT,0);
        this.add(STAGE_COUNT,0);
        if(stage==1){
            //pasang lantai
        lantai.setIcon(new ImageIcon("image/map/Map1/Map001.png"));
        lantai.setSize(800, 600);
        
        for(int i=0;i<16;i++){
            for(int j=0;j<11;j++){
                //pasang player
                if(i==13 && j==10){
                    playerx.lblUnit.setLocation(i*50, j*50);
                    this.add(playerx.lblUnit);
                }
                //pasang door
                else if(i==2 && j==0){
                    pintu = new door(i*50,j*50);
                    this.add(pintu.lblUnit);
                }
                //pasang rock kanan
                else if(j%2==0 && (i>7 && i<15) && j!=0 && j!=10){
                    rock rock = new rock(i*50,j*50);
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                //pasang rock kiri
                else if(j%2==1 && (i>0 && i<7) && j!=1){
                    rock rock = new rock(i*50,j*50);
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }

                //pasang enemy
                else if( (j%2==1 && i==14 && (j>2 &&j<8)) || (j%2==0 && i==1 && (j>2 &&j<9)) ){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang tree
                else if(i==0 || j==0 || i==15 || j==10 ){
                    tree hard_wall = new tree(i*50,j*50);
                    this.add(hard_wall.lblUnit);
                    this.tree.add(hard_wall);  
                }
            }
        }
        
        this.add(lantai);
        }
        else if(stage==2){
            //pasang lantai
        lantai.setIcon(new ImageIcon("image/map/Map2/Map002.png"));
        lantai.setSize(800, 600);
        
        for(int i=0;i<16;i++){
            for(int j=0;j<11;j++){
                //pasang player
                if(i==1 && j==10){
                    playerx.lblUnit.setLocation(i*50, j*50);
                    this.add(playerx.lblUnit);
                }
                //pasang door
                else if(i==10 && j==1){
                    pintu = new door(i*50,j*50);
                    pintu.lblUnit.setIcon(new ImageIcon("image/map/map2/crack.png"));
                    this.add(pintu.lblUnit);
                }
                //pasang rock kanan
                else if(j%2==0 && (i>8 && i<13) && j!=0 && j!=10){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map2/barrel2.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                //pasang rock kiri
                else if(j%2==1 && (i>2 && i<7) && j!=1){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map2/barrel2.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }

                //pasang enemy kanan
                else if( (j%2==1 && i==11 && (j>2 &&j<8))){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang enemy kiri
                else if((j%2==0 && i==5 && (j>2 &&j<9)) ){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang tree
                else if(i==0 || j==0 || i==15 || j==10 ){
                    tree hard_wall = new tree(i*50,j*50);
                    hard_wall.lblUnit.setIcon(new ImageIcon("image/map/map2/barrel1.png"));
                    this.add(hard_wall.lblUnit);
                    this.tree.add(hard_wall);  
                }
            }
        }
        
        this.add(lantai);
        }
        
        else if(stage==3){
            //pasang lantai
        lantai.setIcon(new ImageIcon("image/map/Map3/Map003.png"));
        lantai.setSize(800, 600);
        
        for(int i=0;i<16;i++){
            for(int j=0;j<11;j++){
                //pasang player
                if(i==14 && j==10){
                    playerx.lblUnit.setLocation(i*50, j*50);
                    this.add(playerx.lblUnit);
                }
                //pasang door
                else if(i==1 && j==1){
                    pintu = new door(i*50,j*50);
                    pintu.lblUnit.setIcon(new ImageIcon("image/map/map3/wall.png"));
                    this.add(pintu.lblUnit);
                }
                //pasang rock kanan
                else if(j%2==0 && (i>10 && i<15) && j!=0 && j!=10){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map3/lava_rock.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                //pasang rock tengah
                else if(j%2==1 && (i>5 && i<10) && j!=1){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map3/lava_rock.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                //pasang rock kiri
                else if(j%2==0 && (i>0 && i<5) && j!=0 && j!=10){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map3/lava_rock.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                //pasang enemy kanan
                else if( (j%2==1 && i==14 && (j>2 &&j<6))){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang enemy tengah
                else if((j%2==0 && i==7 && (j>2 &&j<7)) ){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang enemy kiri
                else if( (j%2==1 && i==1 && (j>2 &&j<6))){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                //pasang tree
                else if(i==0 || j==0 || i==15 || j==10 ){
                    tree hard_wall = new tree(i*50,j*50);
                    hard_wall.lblUnit.setIcon(new ImageIcon("image/map/map3/lava_wall.png"));
                    this.add(hard_wall.lblUnit);
                    this.tree.add(hard_wall);  
                }
            }
        }
        
        this.add(lantai);
        }
        
        else if(stage==4){
            //pasang lantai
        lantai.setIcon(new ImageIcon("image/map/Map4/Map004.png"));
        lantai.setSize(800, 600);
        
        for(int i=0;i<16;i++){
            for(int j=0;j<11;j++){
                //pasang player
                if(i==1 && j==10){
                    playerx.lblUnit.setLocation(i*50, j*50);
                    this.add(playerx.lblUnit,0);
                }
                //pasang door
                else if(i==13 && j==1){
                    pintu = new door(i*50,j*50);
                    pintu.lblUnit.setIcon(new ImageIcon("image/map/map4/ice_hole.png"));
                    this.add(pintu.lblUnit,0);
                }
                //pasang rock 
                else if(i%2==0 && (j>0 && i<15) && i!=0 && j!=10){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map4/tentacle.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                
                //pasang enemy kanan
                else if( (j%2==1 && i%2==1  && i!=15 && j==5 && i!=1)){
                    unit enemy = new enemy_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit);
                    this.unit.add(enemy);  
                }
                
                //pasang tree
                else if(i==0 || j==0 || i==15 || j==10 ){
                    tree hard_wall = new tree(i*50,j*50);
                    hard_wall.lblUnit.setIcon(new ImageIcon("image/map/map4/tentacle_wall.png"));
                    this.add(hard_wall.lblUnit);
                    this.tree.add(hard_wall);  
                }
            }
        }
        
        this.add(lantai);
        }
        
        else if(stage==5){
                //pasang lantai
        lantai.setIcon(new ImageIcon("image/map/Map5/Map005.png"));
        lantai.setSize(800, 600);
        
        for(int i=0;i<16;i++){
            for(int j=0;j<11;j++){
                //pasang player
                if(i==7 && j==1){
                    playerx.lblUnit.setLocation(i*50, j*50);
                    this.add(playerx.lblUnit,0);
                }
                //pasang door win
                else if(i==13 && j==1){
                    pintu = new door(i*50,j*50);
                    pintu.lblUnit.setIcon(new ImageIcon("image/map/map5/summoning_circle.png"));
                    this.add(pintu.lblUnit,0);
                }
                //pasang door shop
                else if(i==2 && j==1){
                    shop = new door(i*50,j*50);
                    shop.lblUnit.setIcon(new ImageIcon("image/map/map5/summoning_circle_shop.png"));
                    this.add(shop.lblUnit,0);
                }
                //pasang rock 
                else if(i==1 || i==14 || j==2 || j==9){
                    rock rock = new rock(i*50,j*50);
                    rock.lblUnit.setIcon(new ImageIcon("image/map/map5/wall_broke_2.png"));
                    this.add(rock.lblUnit);
                    this.rock.add(rock);
                }
                
                //pasang boss
                else if(i==7 && j==3){
                    unit enemy = new enemy_boss_stage(i*50,j*50,load);
                    this.add(enemy.lblUnit,0);
                    this.unit.add(enemy);  
                }
                
                //pasang tree
                else if(i==0 || j==0 || i==15 || j==10 ){
                    tree hard_wall = new tree(i*50,j*50);
                    hard_wall.lblUnit.setIcon(new ImageIcon("image/map/map5/statue.png"));
                    this.add(hard_wall.lblUnit);
                    this.tree.add(hard_wall);  
                }
            }
        }
        
        this.add(lantai);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        STAGE_COUNT = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        LIFE_COUNT = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        GOLD_COUNT = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("STAGE : ");

        STAGE_COUNT.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        STAGE_COUNT.setForeground(new java.awt.Color(255, 255, 255));

        jLabel2.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("LIFE : ");

        LIFE_COUNT.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        LIFE_COUNT.setForeground(new java.awt.Color(255, 255, 255));

        jLabel3.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("GOLD : ");

        GOLD_COUNT.setFont(new java.awt.Font("Good Times", 3, 24)); // NOI18N
        GOLD_COUNT.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(167, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(GOLD_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(LIFE_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(STAGE_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addContainerGap(570, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(STAGE_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(GOLD_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIFE_COUNT, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents
public void cek_life_and_score(){
    NewJFrame b= (NewJFrame) this.getParent().getParent().getParent().getParent();
        if(b.player.getLife()<=0){
            
            b.go_to_game_over();
        }
        LIFE_COUNT.setText(b.player.getLife()+"");
        GOLD_COUNT.setText(b.getGold()+"");
    }



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel GOLD_COUNT;
    private javax.swing.JLabel LIFE_COUNT;
    private javax.swing.JLabel STAGE_COUNT;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    // End of variables declaration//GEN-END:variables
}
