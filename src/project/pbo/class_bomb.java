/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project.pbo;

import java.awt.Image;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author edwin
 */
class bomb extends Thread implements Runnable{
    JLabel lblUnit;
    JLabel up;
    JLabel down;
    JLabel left;
    JLabel right;
    STAGE_MODE stage;
    ImageIcon gambar;
    int x;
    int y;
    public bomb(int x,int y,STAGE_MODE map){
        this.x = x;
        this.y = y;
        ImageIcon rawImage = new ImageIcon("image/bomb/big_bomb.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.up = new JLabel(this.gambar);
        this.up.setBounds(x, y-50, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.down = new JLabel(this.gambar);
        this.down.setBounds(x, y+50, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.left = new JLabel(this.gambar);
        this.left.setBounds(x-50, y, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.right = new JLabel(this.gambar);
        this.right.setBounds(x+50, y, 50, 50);
        
        this.stage=map;
    }

    
    
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void run() {
        int meledak=0;
        NewJFrame b=(NewJFrame) stage.getParent().getParent().getParent().getParent();
        ArrayList<rock>toRemove=new ArrayList<rock>();
        ArrayList<unit>toRemove2=new ArrayList<unit>();
        int rand=0;
        while(meledak<5){
            
            try {
                if(meledak==3){
                    this.lblUnit.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.up.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.down.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.left.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.right.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    System.out.println(b.player.getLife());
                        if (b.player.lblUnit.getX()==up.getX() && b.player.lblUnit.getY()>=up.getY()&&b.player.lblUnit.getY()<=down.getY()){
                            if(b.player.getLife()>0){
                                b.player.setLife(b.player.getLife()-1);
                            }
                        }
                        else if (b.player.lblUnit.getX()==down.getX() && b.player.lblUnit.getY()>=up.getY()&&b.player.lblUnit.getY()<=down.getY()){
                            if(b.player.getLife()>0){
                                b.player.setLife(b.player.getLife()-1);
                            }

                        }
                        else if (b.player.lblUnit.getX()>=left.getX()&&b.player.lblUnit.getX()<=right.getX()&&b.player.lblUnit.getY()==left.getY()){
                            if(b.player.getLife()>0){
                                b.player.setLife(b.player.getLife()-1);
                            }
                        }
                    for (rock x : b.STAGE.rock) {
                        if (x.lblUnit.getX()==up.getX()&&x.lblUnit.getY()==up.getY()){
                            b.STAGE.remove(x.lblUnit);
                            rand=(int) (Math.random()*101);
                            if(rand<=50){
                                b.setGold(b.getGold()+10);
                            }
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==down.getX()&&x.lblUnit.getY()==down.getY()){
                            b.STAGE.remove(x.lblUnit);
                            rand=(int) (Math.random()*101);
                            if(rand<=50){
                                b.setGold(b.getGold()+10);
                            }
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==left.getX()&&x.lblUnit.getY()==left.getY()){
                            b.STAGE.remove(x.lblUnit);
                            rand=(int) (Math.random()*101);
                            if(rand<=50){
                                b.setGold(b.getGold()+10);
                            }
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==right.getX()&&x.lblUnit.getY()==right.getY()){
                            b.STAGE.remove(x.lblUnit);
                            rand=(int) (Math.random()*101);
                            if(rand<=50){
                                b.setGold(b.getGold()+10);
                            }
                            toRemove.add(x);
                        }
                    }
                    for(rock x:toRemove){
                        b.STAGE.rock.remove(x);
                    }
                    
                    for (unit x : b.STAGE.unit) {
                        System.out.println("lol");
                        if (x.lblUnit.getX()==up.getX() && x.lblUnit.getY()>=up.getY()&&x.lblUnit.getY()<=down.getY()){
                            x.stop();
                            b.setGold(b.getGold()+10);
                            b.STAGE.remove(x.lblUnit);
                            toRemove2.add(x);
                        }
                        else if (x.lblUnit.getX()==down.getX() && x.lblUnit.getY()>=up.getY()&&x.lblUnit.getY()<=down.getY()){
                            x.stop();
                            b.setGold(b.getGold()+10);
                            b.STAGE.remove(x.lblUnit);
                            toRemove2.add(x);
                        }
                        else if (x.lblUnit.getX()>=left.getX()&&x.lblUnit.getX()<=right.getX()&&x.lblUnit.getY()==left.getY()){
                            x.stop();
                            b.setGold(b.getGold()+10);
                            b.STAGE.remove(x.lblUnit);
                            toRemove2.add(x);
                        }
                        /*else if (x.lblUnit.getX()==right.getX()&&x.lblUnit.getY()==right.getY()){
                            stage.remove(x.lblUnit);
                            toRemove2.add(x);
                        }*/
                    }
                    for(unit x:toRemove2){
                        b.STAGE.unit.remove(x);
                    }
                    b.STAGE.repaint();
                }
                else if(meledak==4){
                    this.lblUnit.setIcon(new ImageIcon(""));
                    this.down.setIcon(new ImageIcon(""));
                    this.up.setIcon(new ImageIcon(""));
                    this.left.setIcon(new ImageIcon(""));
                    this.right.setIcon(new ImageIcon(""));
                }
                meledak++;
                Thread.sleep(1000);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("meledak");
    }
}

class bomb_endless extends Thread implements Runnable{
    JLabel lblUnit;
    JLabel up;
    JLabel down;
    JLabel left;
    JLabel right;
    STAGE_MODE stage;
    ImageIcon gambar;
    int x;
    int y;
    player player;
    public bomb_endless(player player,int x,int y,STAGE_MODE map){
        this.player = player;
        this.x = x;
        this.y = y;
        ImageIcon rawImage = new ImageIcon("image/bomb/big_bomb.png");
        Image img = rawImage.getImage();
        Image newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.lblUnit = new JLabel(this.gambar);
        this.lblUnit.setBounds(x, y, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.up = new JLabel(this.gambar);
        this.up.setBounds(x, y-50, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.down = new JLabel(this.gambar);
        this.down.setBounds(x, y+50, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.left = new JLabel(this.gambar);
        this.left.setBounds(x-50, y, 50, 50);
        
        rawImage = new ImageIcon("");
        img = rawImage.getImage();
        newImg = img.getScaledInstance(50, 50,Image.SCALE_SMOOTH);
        this.gambar = new ImageIcon(newImg);
        this.right = new JLabel(this.gambar);
        this.right.setBounds(x+50, y, 50, 50);
        
        this.stage=map;
    }

    
    
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    @Override
    public void run() {
        int meledak=0;
        NewJFrame b=(NewJFrame) stage.getParent().getParent().getParent().getParent();
        ArrayList<rock>toRemove=new ArrayList<rock>();
        ArrayList<unit>toRemove2=new ArrayList<unit>();
        while(meledak<5){
            
            try {
                if(meledak==3){
                    this.lblUnit.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.up.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.down.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.left.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    this.right.setIcon(new ImageIcon("image/bomb/explosion.png"));
                    System.out.println(b.ENDLESS.player.getLife());
                        if (b.ENDLESS.player.lblUnit.getX()==up.getX() && b.ENDLESS.player.lblUnit.getY()>=up.getY()&&b.ENDLESS.player.lblUnit.getY()<=down.getY()){
                            if(b.ENDLESS.player.getLife()>0){
                                b.ENDLESS.player.setLife(b.ENDLESS.player.getLife()-1);
                            }
                            else{
                                b.go_to_game_over();
                            }
                        }
                        else if (b.ENDLESS.player.lblUnit.getX()==down.getX() && b.ENDLESS.player.lblUnit.getY()>=up.getY()&&b.ENDLESS.player.lblUnit.getY()<=down.getY()){
                            if(b.ENDLESS.player.getLife()>0){
                                b.ENDLESS.player.setLife(b.ENDLESS.player.getLife()-1);
                            }
                            else{
                                b.go_to_game_over();
                            }
                        }
                        else if (b.ENDLESS.player.lblUnit.getX()>=left.getX()&&b.ENDLESS.player.lblUnit.getX()<=right.getX()&&b.ENDLESS.player.lblUnit.getY()==left.getY()){
                            if(b.ENDLESS.player.getLife()>0){
                                b.ENDLESS.player.setLife(b.ENDLESS.player.getLife()-1);
                            }
                            else{
                                b.go_to_game_over();
                            }
                        }
                    
                    for (rock x : b.ENDLESS.batu) {
                        if (x.lblUnit.getX()==up.getX()&&x.lblUnit.getY()==up.getY()){
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=10;
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==down.getX()&&x.lblUnit.getY()==down.getY()){
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=10;
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==left.getX()&&x.lblUnit.getY()==left.getY()){
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=10;
                            toRemove.add(x);
                        }
                        else if (x.lblUnit.getX()==right.getX()&&x.lblUnit.getY()==right.getY()){
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=10;
                            toRemove.add(x);
                        }
                    }
                    for(rock x:toRemove){
                        b.ENDLESS.batu.remove(x);
                    }
                    
                    int counter_respawn=0;
                    for (unit x : b.ENDLESS.enemy) {
                        System.out.println("lol");
                        if (x.lblUnit.getX()==up.getX() && x.lblUnit.getY()>=up.getY()&&x.lblUnit.getY()<=down.getY()){
                            x.stop();
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=30;
                            toRemove2.add(x);
                            counter_respawn++;
                        }
                        else if (x.lblUnit.getX()==down.getX() && x.lblUnit.getY()>=up.getY()&&x.lblUnit.getY()<=down.getY()){
                            x.stop();
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=30;
                            toRemove2.add(x);
                            counter_respawn++;
                        }
                        else if (x.lblUnit.getX()>=left.getX()&&x.lblUnit.getX()<=right.getX()&&x.lblUnit.getY()==left.getY()){
                            x.stop();
                            b.ENDLESS.remove(x.lblUnit);
                            player.score+=30;
                            toRemove2.add(x);
                            counter_respawn++;
                        }
                        /*else if (x.lblUnit.getX()==right.getX()&&x.lblUnit.getY()==right.getY()){
                            stage.remove(x.lblUnit);
                            toRemove2.add(x);
                        }*/
                    }
                    
                    for(unit x:toRemove2){
                        b.ENDLESS.enemy.remove(x);
                    }
                    for (int i = 0; i < counter_respawn; i++) {
                        b.ENDLESS.respawnEnemy();
                    }
                    b.ENDLESS.repaint();
                }
                else if(meledak==4){
                    this.lblUnit.setIcon(new ImageIcon(""));
                    this.down.setIcon(new ImageIcon(""));
                    this.up.setIcon(new ImageIcon(""));
                    this.left.setIcon(new ImageIcon(""));
                    this.right.setIcon(new ImageIcon(""));
                }
                meledak++;
                Thread.sleep(1000);//miliseconds
            } catch (InterruptedException ex) {
            }
        }
        System.out.println("meledak");
    }
}

